# Surprize Me Test Task
### Babichev Rinat

### Instruction on how to use:

**Disclaimer: Payment - Плажет, Payoff - Выплата**

1. Install requirements: `$ pip install -r requirements.txt`
2. Create database 
 `$ cd backend` , `$ python manage.py migrate`
3. Now you can import three users I created (test@a.ru test@b.ru test@c.ru)
  `$ python manage.py loaddata users.json`, **default password for each - 123 django-superuser is test@a.ru**
4. Then run `$python manage.py fill_payments 10` where 10 is the number of payments you want to add
5. Now you can run my tests `$ python manage.py test payments.tests`
6. If you want you can change POST_REQUEST_URL url in backend/payments/admin.py with yours (currently set to url from task)
7. Now Run `python manage.py runserver`
8. Enjoy http://127.0.0.1/login/ and http://127.0.0.1/admin/

It can be easily converted to Docker with PosgreSQL but I had no time to do it.