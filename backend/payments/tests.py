from unittest.mock import Mock

from django.test import TestCase
from django.test import Client
# Create your tests here.
from django.urls import reverse

from payments.admin import PayoffAdmin
from payments.models import User, Payment, Payoff


class PaymenTestCase(TestCase):
    def testPayment(self):
        """Test that 30% goes back to user"""
        u1 = User.objects.create(email='t1@r.u', first_name='b', last_name='c', password='d')
        self.assertEqual(u1.award, 0)
        p1 = Payment.objects.create(author=u1, sum=1000)
        self.assertEqual(p1.author.award, 300)
        p2 = Payment.objects.create(author=u1, sum=1)
        self.assertEqual(p1.author.award, 300.3)


class PayoffTestCase(TestCase):
    def setUp(self) -> None:
        u1 = User.objects.create(email='t1@r.u', first_name='b', last_name='c', password='d', award=100)
        self.client = Client()
        self.client.force_login(u1)
        self.logged = u1

    def testGetAuth(self):
        """Auth user can get 'send money' page"""
        resp = self.client.get(reverse('pay'))
        assert resp.status_code == 200

    def testCreatePayment(self):
        # correct sum
        resp = self.client.post(reverse('pay'), {'sum': 90})
        self.assertEqual(resp.status_code, 200)
        q = Payoff.objects.filter(sum=90)
        self.assertEqual(len(q), 1)
        self.assertEqual(q[0].user, self.logged)

        # incorrect sum ( > balance)
        resp = self.client.post(reverse('pay'), {'sum': 110})
        q = Payoff.objects.filter(sum=110)
        self.assertEqual(len(q), 0)


class TestAdmin(TestCase):

    def testResponse(self):
        """протестировать создание выплаты"""
        request = Mock()
        request.POST = "_process"
        u1 = User.objects.create(email='t1@r.u', first_name='b', last_name='c', password='d', award=100)
        pf = Payoff.objects.create(sum=89, user=u1, account_number='442211')
        request.user = u1
        PayoffAdmin.response_change(None, request, pf)
        # произошла выплата
        self.assertEqual(request.user.award, 11)
        self.assertEqual(Payoff.objects.all()[0].processed, True)
