from django import forms

from payments.models import Payoff


class PayoffForm(forms.ModelForm):
    account_number = forms.CharField(required=False)

    class Meta:
        model = Payoff
        fields = ['sum', 'account_number']
