from datetime import datetime

import requests
from django.contrib import admin
from django.http import HttpResponseRedirect

from .models import User, Payoff, Payment

# Register your models here.

POST_REQUEST_URL = 'https://webhook.site/36693e00-8f59-4f7b-9a85-1d1e7ddde4d4'


class UserAdmin(admin.ModelAdmin):
    fields = ['email',  'first_name', 'last_name', 'award']


class PayoffAdmin(admin.ModelAdmin):
    readonly_fields = ('creation_date', 'process_date')
    change_form_template = "request_submit.html"

    def response_change(self, request, obj):
        """ Метод обработки выплаты"""
        if "_process" in request.POST:
            obj.processed = True
            if request.user.award >= obj.sum:
                request.user.award -= obj.sum
                request.user.save()
            obj.process_date = datetime.now()
            obj.save()
            if obj.account_number != '' and obj.account_number is not None:
                r = requests.post(POST_REQUEST_URL,
                                  json={"account": obj.account_number, "amount": obj.sum}
                                  )
            return HttpResponseRedirect(".")
        return super().response_change(request, obj)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['processed'] = self.get_queryset(request).get(id=object_id).processed
        return super().change_view(
            request, object_id, form_url, extra_context=extra_context,
        )

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.processed: # "editable"
            return self.readonly_fields + ('sum', 'user', 'account_number', 'processed')
        return self.readonly_fields


admin.site.register(User, UserAdmin)
admin.site.register(Payment)
admin.site.register(Payoff, PayoffAdmin)
