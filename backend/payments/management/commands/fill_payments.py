from random import randint
from django.core.management.base import BaseCommand, CommandError
from payments.models import User, Payment


class Command(BaseCommand):
    help = 'Fill database with payments'

    def add_arguments(self, parser):
        parser.add_argument('count', nargs='?', type=int)

    def handle(self, *args, **options):
        count = options['count']
        users = User.objects.all()
        for i in range(count):
            rand_id = randint(0, len(users) - 1)
            author = users[rand_id]
            sum = randint(1, 100)
            p = Payment(author=author, sum=sum)
            p.save()
        self.stdout.write(self.style.SUCCESS(f'Successfully filled {count} payments'))
