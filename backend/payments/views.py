from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views import View

from payments.forms import PayoffForm


class PayView(View):

    @method_decorator(login_required(login_url='/login/'))
    def get(self, request):
        user = request.user
        payoff = PayoffForm()
        return render(request, 'pay.html',
                      context={'form': payoff, 'user': user})

    @method_decorator(login_required(login_url='/login/'))
    def post(self, request):
        payoff = PayoffForm(request.POST)
        user = request.user

        if payoff.is_valid() and user.award >= payoff.cleaned_data.get("sum"):
            res = payoff.save(commit=False)
            res.user = user
            res.save()
        else:
            payoff.add_error(None, "Provided sum is incorrect")
        return render(request, 'pay.html',
                      context={'form': payoff, 'user': user})
