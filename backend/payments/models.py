from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.core.validators import MinValueValidator
from django.db.models import EmailField, CharField, FloatField, ForeignKey, DateField, Model, BooleanField, \
    DateTimeField
from django.utils.translation import ugettext_lazy as _


class CustomUserManager(BaseUserManager):
    """
    UserManager with email primary field instead of username
    """

    def create_user(self, email, password, first_name, last_name, **extra_fields):
        """
        Create and save a User with the given email and password.
        """
        if not email:
            raise ValueError(_('The Email must be set'))
        if not first_name:
            raise ValueError(_('The first name must be set'))
        if not last_name:
            raise ValueError(_('The last name must be set'))

        email = self.normalize_email(email)
        user = self.model(email=email, first_name=first_name, last_name=last_name, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, first_name, last_name, **extra_fields):
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)
        return self.create_user(email, password, first_name, last_name, **extra_fields)


class User(AbstractUser):
    """
        Пользователь
    """
    username = None
    objects = CustomUserManager()

    phone = CharField(max_length=30, null=True)
    award = FloatField(default=0)
    first_name = CharField(max_length=30)
    last_name = CharField(max_length=30)
    email = EmailField(unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    @property
    def active_payoffs_count(self):
        return Payoff.objects.filter(processed=False, user=self).count


class Payment(Model):
    """
        Платеж
    """
    author = ForeignKey(User)
    sum = FloatField(default=0, validators=[MinValueValidator(0)])
    creation_date = DateTimeField(auto_now=True)

    def pay(self):
        if self.id is None:
            # only on new payment creation
            self.author.award += 0.3 * self.sum
            self.author.save()

    def save(self, *args, **kwargs):
        """ Read only model """
        self.pay()
        super(Payment, self).save(*args, **kwargs)

    def __unicode__(self):
        return f'{self.sum} from {self.author.email} user'


class Payoff(Model):
    """
        Выплата
    """
    user = ForeignKey(User)
    sum = FloatField(default=0)
    creation_date = DateTimeField(auto_now=True)
    process_date = DateTimeField(null=True, blank=True)
    processed = BooleanField(default=False)
    account_number = CharField(max_length=30, null=True, blank=True)

    def __unicode__(self):
        return f'{self.user} - {self.sum} - {self.account_number}'
